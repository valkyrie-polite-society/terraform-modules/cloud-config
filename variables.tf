variable "write_files" {
  type        = map(string)
  default     = {}
  description = "A map of files to create. In the map, each key is the absolute path to write the file and the value is the content of the file."
}
