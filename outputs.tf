output "yaml" {
  value = templatefile("${path.module}/cloud-init.yaml", {
    write_files = var.write_files
  })
}
